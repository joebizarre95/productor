import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';


import { MyApp } from './app.component';
import { HomePageModule } from '../pages/home/home.module';
import { SelectionPageModule } from '../pages/selection/selection.module';
import { BgpcarnePageModule } from '../pages/bgpcarne/bgpcarne.module';
import { RegistrosPageModule } from '../pages/registros/registros.module';
import { RegmedicosPageModule } from '../pages/regmedicos/regmedicos.module';
import { ReganimalesPageModule } from '../pages/reganimales/reganimales.module';
import { ModalpagePageModule } from '../pages/modalpage/modalpage.module';
import { InventariomedPageModule } from '../pages/inventariomed/inventariomed.module';
import { DocumentsPageModule } from '../pages/documents/documents.module';
import { NewdocumentPageModule } from '../pages/newdocument/newdocument.module';
import { BibliotecaPageModule } from  '../pages/biblioteca/biblioteca.module';
import { ProtocolosPageModule } from '../pages/protocolos/protocolos.module';
import { ConfigurationsPageModule } from '../pages/configurations/configurations.module';
import { ErrorReportingPageModule } from '../pages/error-reporting/error-reporting.module';
import { LogRegistrosPageModule } from '../pages/log-registros/log-registros.module';
import { DetallesmodalPageModule } from '../pages/detallesmodal/detallesmodal.module';
import { CalendarioPageModule } from '../pages/calendario/calendario.module';
import { DetalledocumentoPageModule } from '../pages/detalledocumento/detalledocumento.module';
import { DocumentosPageModule } from '../pages/documentos/documentos.module';

//providers
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileTransferObject } from '@ionic-native/file-transfer'; 
import { DocumentViewer } from '@ionic-native/document-viewer';
import { Camera } from '@ionic-native/camera';
import { HttpProvider } from '../providers/http/http';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { SQLite } from '@ionic-native/sqlite';
import { TaskServiceProvider } from '../providers/task-service/task-service';
import { ExportProvider } from '../providers/export/export';
import { Calendar } from '@ionic-native/calendar';
//import { FilePath } from '@ionic-native/file-path';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    //modulos del sistema
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
   


    //modulos de paginas
    HomePageModule,
    SelectionPageModule,
    BgpcarnePageModule,
    RegistrosPageModule,
    RegmedicosPageModule,
    ReganimalesPageModule,
    ModalpagePageModule,
    InventariomedPageModule,
    DocumentsPageModule,
    NewdocumentPageModule,
    BibliotecaPageModule,
    ProtocolosPageModule,
    ConfigurationsPageModule,
    ErrorReportingPageModule,
    LogRegistrosPageModule,
    DetallesmodalPageModule,
    CalendarioPageModule,
    DetalledocumentoPageModule,
    DocumentosPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileChooser,
    FileOpener,
    FilePath,
    File,
    DocumentViewer,
    Calendar,
    FileTransfer,
    FileTransferObject,
    Camera,
    Network,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpProvider,
    TaskServiceProvider,
    ExportProvider,
  ]
})
export class AppModule {}
