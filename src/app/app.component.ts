import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite } from '@ionic-native/sqlite';

import { HomePage } from '../pages/home/home';
//proviers 
import { TaskServiceProvider } from '../providers/task-service/task-service';
import { ToastController } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, 
   public statusBar: StatusBar,
    public splashScreen: SplashScreen,
       public sqlite : SQLite ,
        public taskService :  TaskServiceProvider  , 
         public toastCtrl :  ToastController, ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.createDatabase();
     
    });
  }


  private createDatabase(){
    this.sqlite.create({
      name: 'data.db',
      location : 'default'
    })

    .then((db) => {
        this.taskService.setDatabase(db);
        return this.taskService.TableUser();
    })
    .then((db) => {
        return this.taskService.TableNormasUser();  
    })
    .then((db) => {
        return this.taskService.RegEstatic();  
    })
     .then((db) => {
       return this.taskService.ResponseReg();
    })
    .then((db) => {
        return this.taskService.RegEstaticArchivos();
    })
    .then((db) => {
        return this.taskService.TableProtocolo();
    })
    .then((db) => {
        return this.taskService.TableBiblioteca();
    })

    .then((db)=>{
       return this.taskService.newData();

    }).then((db)=>{
       return this.taskService.newData2();

    }).then((db)=>{
       return this.taskService.newData3();
    })
    .then((db)=>{
       return this.taskService.newData4();
    })
    .then((db)=>{
       return this.taskService.newData5();
    })

      .then(() => {
        this.splashScreen.hide();
        this.rootPage = 'HomePage';

        let toast = this.toastCtrl.create({
          message : 'Base de Datos Abierta',
          duration : 4000,
          position : 'bottom',
        });

        toast.present();

      })

      .catch(error => {
          let toast = this.toastCtrl.create({
            message: 'Base de datos erronea' + JSON.stringify(error),
            duration : 4000,
            position : 'bottom',
          });

          toast.present();
      })
  }

  //base de datos para la aplciacion


}

