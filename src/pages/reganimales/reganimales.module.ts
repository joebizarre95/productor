import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReganimalesPage } from './reganimales';

@NgModule({
  declarations: [
    ReganimalesPage,
  ],
  imports: [
    IonicPageModule.forChild(ReganimalesPage),
  ],
})
export class ReganimalesPageModule {}
