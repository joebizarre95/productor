import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';


//import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the ReganimalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reganimales',
  templateUrl: 'reganimales.html',
})
export class ReganimalesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReganimalesPage');
  }

  guardar(){
  	let toast = this.toastCtrl.create({
  		message: 'Espere mientras se Guardan los Datos',
  		duration: 3000,
  		position:'top'
  	});

  	 toast.onDidDismiss(() => {
 		this.navCtrl.pop();
  });

  toast.present();
  }


  getPhoto(){
  /*  const cameraOptions : CameraOptions = {
      quality:80,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      //saveToPhotoAlmbum: false,
    };
    
    this.camera.getPicture(cameraOptions).then((imagenData) => {
      alert(imagenData);
    }).catch(err => console.log(err));*/
    console.log('activacion de la camara');
  }

}
