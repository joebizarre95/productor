var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
//import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the ReganimalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ReganimalesPage = /** @class */ (function () {
    function ReganimalesPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
    }
    ReganimalesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ReganimalesPage');
    };
    ReganimalesPage.prototype.guardar = function () {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: 'Espere mientras se Guardan los Datos',
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.pop();
        });
        toast.present();
    };
    ReganimalesPage.prototype.getPhoto = function () {
        /*  const cameraOptions : CameraOptions = {
            quality:80,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: false,
            encodingType: this.camera.EncodingType.JPEG,
            //saveToPhotoAlmbum: false,
          };
          
          this.camera.getPicture(cameraOptions).then((imagenData) => {
            alert(imagenData);
          }).catch(err => console.log(err));*/
        console.log('activacion de la camara');
    };
    ReganimalesPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-reganimales',
            templateUrl: 'reganimales.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ToastController])
    ], ReganimalesPage);
    return ReganimalesPage;
}());
export { ReganimalesPage };
//# sourceMappingURL=reganimales.js.map