import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { AlertController } from 'ionic-angular';
//components
//import { SelectionPage } from '../selection/selection';
import { BgpcarnePage } from '../bgpcarne/bgpcarne';


//providers
import { HttpProvider } from '../../providers/http/http';
import { TaskServiceProvider } from '../../providers/task-service/task-service';
//import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions } from '@angular/http';


@IonicPage(
  )
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers : [ TaskServiceProvider ]
})
export class HomePage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public httpProvider: HttpProvider, 
              public http : Http,
              public loadingCtrl : LoadingController ,
              public network: Network,
              public alertCtrl : AlertController, 
              public tasksService: TaskServiceProvider,) {
  }

  ionViewDidLoad() {
   
  }


   task: any[] = []; //elementos para la base de datos
   datos = {} //datos de los inputs
    usuarios : any []; //array dodne se guarda la data del usuario
     signing(){

       var headers = new Headers();
       headers.append("Accept",'application/json');
       headers.append('Content-Type', 'application/x-www-form-urlencoded');
       //options 
        let options = new RequestOptions({ headers : headers });
        let datos = this.datos // => datos = {}
        let urlpost = 'http://127.0.0.1:8000/api/PostAPI/';
     //llenado del servicio
     if (this.network.type != 'none') {
     // code...
         //se envia la peticion al servidor
         let response = this.httpProvider.sendpostToServer(datos,urlpost,options);
         console.log(response);
         let loader = this.loadingCtrl.create({
           content: "Sincronizando datos con el servidor!",
           //opcion para que sea mientras carga la data
         });
          loader.present();
            //mientras tanto luego se eliminara
              setTimeout(() => {
              loader.dismiss();
            }, 5000);


           if(response){
             let loader = this.loadingCtrl.create({
             content: "Hubo un error intentar de nuevo en breves minutos!",
           });
             loader.present();
                setTimeout(() => {
                loader.dismiss();
              }, 5000);
           }else{
              let loader = this.loadingCtrl.create({
               content: "descargando datos!"
               });
              loader.present();
               setTimeout(() => {

                loader.dismiss();
                 this.navCtrl.push(BgpcarnePage)
              }, 5000);
              

             }  
               
     }else{

         let alert = this.alertCtrl.create({
           title : 'Ups!',
           subTitle : 'En estos instantes no posees conexion a internet pero de igual manera podras usar la aplicacion',
           buttons: ['OK']
         });
         alert.present();

      
         }
     }//fin de la funcion 

     
     get_last_user(){
         let urlget = 'http://127.0.0.1:8000/api/UserAPI/';
         let datos_usuario = this.httpProvider.getdataForUser(urlget);
         console.log(datos_usuario);
     }

     next_page(){
       this.navCtrl.push(BgpcarnePage)
     }

}
