var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { AlertController } from 'ionic-angular';
//components
//import { SelectionPage } from '../selection/selection';
import { BgpcarnePage } from '../bgpcarne/bgpcarne';
//providers
import { HttpProvider } from '../../providers/http/http';
import { TaskServiceProvider } from '../../providers/task-service/task-service';
//import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions } from '@angular/http';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, httpProvider, http, loadingCtrl, network, alertCtrl, tasksService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.network = network;
        this.alertCtrl = alertCtrl;
        this.tasksService = tasksService;
        this.task = []; //elementos para la base de datos
        this.datos = {}; //datos de los inputs
    }
    HomePage.prototype.ionViewDidLoad = function () {
    };
    HomePage.prototype.signing = function () {
        var _this = this;
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        //options 
        var options = new RequestOptions({ headers: headers });
        var datos = this.datos; // => datos = {}
        var urlpost = 'http://127.0.0.1:8000/api/PostAPI/';
        //llenado del servicio
        if (this.network.type != 'none') {
            // code...
            //se envia la peticion al servidor
            var response = this.httpProvider.sendpostToServer(datos, urlpost, options);
            console.log(response);
            var loader_1 = this.loadingCtrl.create({
                content: "Sincronizando datos con el servidor!",
            });
            loader_1.present();
            //mientras tanto luego se eliminara
            setTimeout(function () {
                loader_1.dismiss();
            }, 5000);
            if (response) {
                var loader_2 = this.loadingCtrl.create({
                    content: "Hubo un error intentar de nuevo en breves minutos!",
                });
                loader_2.present();
                setTimeout(function () {
                    loader_2.dismiss();
                }, 5000);
            }
            else {
                var loader_3 = this.loadingCtrl.create({
                    content: "descargando datos!"
                });
                loader_3.present();
                setTimeout(function () {
                    loader_3.dismiss();
                    _this.navCtrl.push(BgpcarnePage);
                }, 5000);
                this.tasksService.open_database();
                /* this.tasksService.create_logs(datos)
                 .then(response =>{
                     //cuando se guarden los datos pasar a la otra pagina
                 }).catch(error => {
                   console.error(error);
                 })*/
            }
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Ups!',
                subTitle: 'En estos instantes no posees conexion a internet pero de igual manera podras usar la aplicacion',
                buttons: ['OK']
            });
            alert_1.present();
            //comenzar a guardar toda la data del nuevo log en sqlite //datos de los inputs
            this.tasksService.open_database();
            /* this.tasksService.create_logs(datos)
             .then(response =>{
                 //cuando se guarden los datos pasar a la otra pagina
             }).catch(error => {
               console.error(error);
             })
           
            */
        }
    }; //fin de la funcion 
    HomePage.prototype.get_last_user = function () {
        var urlget = 'http://127.0.0.1:8000/api/UserAPI/';
        var datos_usuario = this.httpProvider.getdataForUser(urlget);
        console.log(datos_usuario);
    };
    HomePage.prototype.next_page = function () {
        this.navCtrl.push(BgpcarnePage);
    };
    HomePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-home',
            templateUrl: 'home.html',
            providers: [TaskServiceProvider]
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            HttpProvider,
            Http,
            LoadingController,
            Network,
            AlertController,
            TaskServiceProvider])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map