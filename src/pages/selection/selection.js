var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { BgpcarnePage } from '../bgpcarne/bgpcarne';
import { HomePage } from '../home/home';
import { TaskServiceProvider } from '../../providers/task-service/task-service';
/**
 * Generated class for the SelectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SelectionPage = /** @class */ (function () {
    function SelectionPage(navCtrl, navParams, alertCtrl, loadingCtrl, homepage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.homepage = homepage;
    }
    SelectionPage.prototype.ionViewDidLoad = function () {
    };
    SelectionPage.prototype.salir = function () {
        this.navCtrl.pop();
    };
    SelectionPage.prototype.bgpcarne = function () {
        this.navCtrl.push(BgpcarnePage);
    };
    SelectionPage.prototype.bovinos = function () {
        console.log('bovinos');
        var alert = this.alertCtrl.create({
            title: '',
            message: 'Comprobando Permisos Espere un momento Porfavor',
        });
        alert.present();
        setTimeout(function () {
            alert.dismiss();
        }, 3000);
    };
    SelectionPage.prototype.buenaspract = function () {
        console.log('buenaspract');
        var alert = this.alertCtrl.create({
            title: '',
            message: 'Comprobando Permisos Espere un momento Porfavor',
        });
        alert.present();
        setTimeout(function () {
            alert.dismiss();
        }, 3000);
    };
    SelectionPage.prototype.cerdos = function () {
        console.log('cerdos');
        var alert = this.alertCtrl.create({
            title: '',
            message: 'Comprobando Permisos Espere un momento Porfavor',
        });
        alert.present();
        setTimeout(function () {
            alert.dismiss();
        }, 3000);
    };
    SelectionPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-selection',
            templateUrl: 'selection.html',
            providers: [TaskServiceProvider]
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            AlertController,
            LoadingController,
            HomePage])
    ], SelectionPage);
    return SelectionPage;
}());
export { SelectionPage };
//# sourceMappingURL=selection.js.map