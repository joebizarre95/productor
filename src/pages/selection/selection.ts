import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { BgpcarnePage } from '../bgpcarne/bgpcarne';
import { HomePage } from '../home/home';
import { TaskServiceProvider } from '../../providers/task-service/task-service';

/**
 * Generated class for the SelectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selection',
  templateUrl: 'selection.html',
  providers : [ TaskServiceProvider ]
})
export class SelectionPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
               public alertCtrl:AlertController, 
               public loadingCtrl : LoadingController,
               public homepage : HomePage ,) {
  }

  ionViewDidLoad() {
   
  }


  salir(){
    this.navCtrl.pop();
  }

  bgpcarne(){

          this.navCtrl.push(BgpcarnePage);
   
  }

  bovinos(){
  	console.log('bovinos');

  	let alert = this.alertCtrl.create({
  			title:'',
  			message : 'Comprobando Permisos Espere un momento Porfavor',
  		});
  		alert.present();
  		setTimeout(()=>{
           alert.dismiss();
  		}, 3000);


  }

  buenaspract(){
  	console.log('buenaspract');

  	let alert = this.alertCtrl.create({
  			title:'',
  			message : 'Comprobando Permisos Espere un momento Porfavor',
  		});
  		alert.present();
  		setTimeout(()=>{
  				alert.dismiss();
  		}, 3000);
  }

  cerdos(){
  	console.log('cerdos');

  	let alert = this.alertCtrl.create({
  			title:'',
  			message : 'Comprobando Permisos Espere un momento Porfavor',
  		});
  		alert.present();
  		setTimeout(()=>{
  				alert.dismiss();
  		}, 3000);
  }

}
