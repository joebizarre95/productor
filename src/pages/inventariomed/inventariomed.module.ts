import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InventariomedPage } from './inventariomed';

@NgModule({
  declarations: [
    InventariomedPage,
  ],
  imports: [
    IonicPageModule.forChild(InventariomedPage),
  ],
})
export class InventariomedPageModule {}
