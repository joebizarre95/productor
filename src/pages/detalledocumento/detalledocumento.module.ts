import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalledocumentoPage } from './detalledocumento';

@NgModule({
  declarations: [
    DetalledocumentoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalledocumentoPage),
  ],
})
export class DetalledocumentoPageModule {}
