import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskServiceProvider } from '../../providers/task-service/task-service';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the DetalledocumentoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalledocumento',
  templateUrl: 'detalledocumento.html',
})
export class DetalledocumentoPage {

  documentos : any ;

  constructor(public navCtrl: NavController, 
  					public navParams: NavParams, 
  						public task : TaskServiceProvider,
  							public toastCtrl : ToastController, ) {

    this.task.newData5()
      

  }

  ionViewDidLoad() {
    this.task.listarDocumentosxReg()
    	.then(resultado =>{
    		this.documentos = resultado
        console.log(resultado);

    		let toast = this.toastCtrl.create({
    			message : 'objetos obtenidos' + JSON.stringify(resultado),
    			position:'bottom',
    			duration : 4000,
    		});

    		toast.present();

    	}).catch((error)=>{
    		let toast = this.toastCtrl.create({
    			message : 'Hubo un error' + JSON.stringify(error),
    			position:'bottom',
    			duration : 4000,
    		});
    		toast.present();
    	});
  }




}
