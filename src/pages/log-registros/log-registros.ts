import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ModalController } from 'ionic-angular';

import { DetallesmodalPage } from '../detallesmodal/detallesmodal';

/**
 * Generated class for the LogRegistrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-log-registros',
  templateUrl: 'log-registros.html',
})
export class LogRegistrosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl : ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogRegistrosPage');
  }

  volver(){
  	this.navCtrl.pop();
  }

  modal(){
  	let modal = this.modalCtrl.create(DetallesmodalPage);
    modal.present();
  }

}
