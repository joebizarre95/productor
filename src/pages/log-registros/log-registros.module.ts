import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogRegistrosPage } from './log-registros';

@NgModule({
  declarations: [
    LogRegistrosPage,
  ],
  imports: [
    IonicPageModule.forChild(LogRegistrosPage),
  ],
})
export class LogRegistrosPageModule {}
