var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewdocumentPage } from '../newdocument/newdocument';
//proviers
import { SQLite } from '@ionic-native/sqlite';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { ModalController } from 'ionic-angular';
/*import { ModalpagePage } from '../modalpage/modalpage';*/
//providers
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
//import { FilePath } from '@ionic-native/file-path';
/**
 * Generated class for the DocumentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DocumentsPage = /** @class */ (function () {
    function DocumentsPage(navCtrl, navParams, documentViewer, modalCtrl, fileChooser, fileOpener, sqlite) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.documentViewer = documentViewer;
        this.modalCtrl = modalCtrl;
        this.fileChooser = fileChooser;
        this.fileOpener = fileOpener;
        this.sqlite = sqlite;
    }
    DocumentsPage.prototype.ionViewDidLoad = function () {
        /*this.sqlite.create({
          name : 'data.db',
          location : 'default'
        }).then((db : SQLiteObject) => {
            db.executeSql('SELECT * FROM documents', {})
              .then(( response ) => {
                this.document = response
              }).catch(error => {
                console.log(error);
              });
        });
   
        */
    };
    DocumentsPage.prototype.volver = function () {
        this.navCtrl.popToRoot();
    };
    DocumentsPage.prototype.openDocument = function () {
        this.fileOpener.open('../../assets/myFile.pdf', 'application/pdf')
            .then(function (file) { return console.log('file is opened'); })
            .catch(function (e) { return console.log('Error opening file', e); });
    };
    DocumentsPage.prototype.newDocument = function () {
        var modal = this.modalCtrl.create(NewdocumentPage);
        modal.present();
    };
    DocumentsPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-documents',
            templateUrl: 'documents.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            DocumentViewer,
            ModalController,
            FileChooser,
            FileOpener,
            SQLite])
    ], DocumentsPage);
    return DocumentsPage;
}());
export { DocumentsPage };
//# sourceMappingURL=documents.js.map