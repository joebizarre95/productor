import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewdocumentPage } from '../newdocument/newdocument';

//proviers
import { DocumentViewer } from '@ionic-native/document-viewer';


import { DetalledocumentoPage } from '../detalledocumento/detalledocumento';


import { ModalController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
/*import { ModalpagePage } from '../modalpage/modalpage';*/


//providers
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { TaskServiceProvider } from '../../providers/task-service/task-service';
//import { FilePath } from '@ionic-native/file-path';

/**
 * Generated class for the DocumentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-documents',
  templateUrl: 'documents.html',
})
export class DocumentsPage {
  documents : any;
  constructor(public navCtrl: NavController,
                 public navParams: NavParams,
                     public documentViewer: DocumentViewer,
                        public modalCtrl : ModalController,
                          public fileChooser : FileChooser,
                            public fileOpener : FileOpener, 
                              public taskService : TaskServiceProvider,
                                public toastCtrl : ToastController,
                              ) {
  }


  ionViewDidLoad() {
     this.taskService.listarRegEstaticos()
       .then((result) => {
         this.documents = result;

         let toast = this.toastCtrl.create({
           message : 'cargando documentos' + JSON.stringify(result),
           duration: 3000,
           position: 'bottom',
         });

         toast.present();

       }).catch((error) => {

         let toast = this.toastCtrl.create({
           message : 'hubo un error al cargar los documentos',
           duration: 3000,
           position: 'bottom',
         });

         toast.present();
       })
   
  }

  volver(){
  	this.navCtrl.pop();
  }


  openDocument(filename){
    this.fileOpener.open(filename, 'application/pdf')
      .then((file) => console.log('file is opened'))
    .catch(e => console.log('Error opening file',e ));
  }



  newDocument(id_registro){
  	let modal = this.modalCtrl.create(NewdocumentPage  , id_registro)
    modal.present();
  }

  detalleDocumento(){
    this.navCtrl.push(DetalledocumentoPage)

  }



}
