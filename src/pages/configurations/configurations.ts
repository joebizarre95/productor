import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



import { ModalController } from 'ionic-angular';

import { ErrorReportingPage } from '../error-reporting/error-reporting';

/**
 * Generated class for the ConfigurationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configurations',
  templateUrl: 'configurations.html',
})
export class ConfigurationsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,  public modalCtrl : ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfigurationsPage');
  }

  error_reporting(){
  	let modal = this.modalCtrl.create(ErrorReportingPage);
    modal.present();
  }

}
