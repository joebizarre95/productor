var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ErrorReportingPage } from '../error-reporting/error-reporting';
/**
 * Generated class for the ConfigurationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConfigurationsPage = /** @class */ (function () {
    function ConfigurationsPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
    }
    ConfigurationsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConfigurationsPage');
    };
    ConfigurationsPage.prototype.error_reporting = function () {
        var modal = this.modalCtrl.create(ErrorReportingPage);
        modal.present();
    };
    ConfigurationsPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-configurations',
            templateUrl: 'configurations.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ModalController])
    ], ConfigurationsPage);
    return ConfigurationsPage;
}());
export { ConfigurationsPage };
//# sourceMappingURL=configurations.js.map