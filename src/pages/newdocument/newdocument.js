var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/*import { ModalController } from 'ionic-angular';

import { ModalpagePage } from '../modalpage/modalpage';*/
import { ToastController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
/**
 * Generated class for the NewdocumentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NewdocumentPage = /** @class */ (function () {
    function NewdocumentPage(navCtrl, navParams, toastCtrl, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.camera = camera;
        this.image = null;
    }
    NewdocumentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewdocumentPage');
    };
    NewdocumentPage.prototype.volver = function () {
        this.navCtrl.pop();
    };
    //para seleccionar algun archvio
    /*
    chosefile(){
      this.fileChooser.open().then(file=>{
        this.filePath.resolvNativePath(file).then(resolveFilePath => {
          this.fileOpener.open(resolvedFilePath, 'application/pdf').then(value => {
            alert('its worked');
          });
        });
      });
    }
  
    */
    NewdocumentPage.prototype.guardar = function () {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: 'Espere mientras se Guardan los Datos',
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.pop();
        });
        toast.present();
    };
    NewdocumentPage.prototype.abrir_camara = function () {
        var _this = this;
        var options = {
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            targetHeight: 1000,
            targetWidth: 1000,
            quality: 100
        };
        this.camera.getPicture(options)
            .then(function (ImageData) {
            _this.image = 'data:image/jpeg;base64,${imageData}';
        }).catch(function (error) {
            console.error(error);
        });
    };
    NewdocumentPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-newdocument',
            templateUrl: 'newdocument.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ToastController, Camera])
    ], NewdocumentPage);
    return NewdocumentPage;
}());
export { NewdocumentPage };
//# sourceMappingURL=newdocument.js.map