import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewdocumentPage } from './newdocument';

@NgModule({
  declarations: [
    NewdocumentPage,
  ],
  imports: [
    IonicPageModule.forChild(NewdocumentPage),
  ],
})
export class NewdocumentPageModule {}
