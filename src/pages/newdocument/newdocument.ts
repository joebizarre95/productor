import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/*import { ModalController } from 'ionic-angular';

import { ModalpagePage } from '../modalpage/modalpage';*/
import { ToastController } from 'ionic-angular';

import {  Headers } from '@angular/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HttpProvider } from '../../providers/http/http';
import { TaskServiceProvider } from '../../providers/task-service/task-service';


import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { FilePath } from '@ionic-native/file-path';

/**
 * Generated class for the NewdocumentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newdocument',
  templateUrl: 'newdocument.html',
})
export class NewdocumentPage {
  id_registro = {}
  constructor(public navCtrl: NavController, 
                    public navParams: NavParams,
                     public toastCtrl : ToastController,
                      public camera: Camera,
                        public http : HttpProvider,  
                          public taskService : TaskServiceProvider,
                            public fileChooser : FileChooser, 
                              public fileOpener : FileOpener, 
                                public filePath : FilePath, ) {

        this.id_registro = navParams.get('id_registro');


        }
  



  ionViewDidLoad() {
    console.log('ionViewDidLoad NewdocumentPage');
  }

  volver(){
  	this.navCtrl.pop();
  }

 
image : string = null;
  abrir_camara(){
    let options : CameraOptions = {
      destinationType : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType : this.camera.PictureSourceType.CAMERA,
      mediaType : this.camera.MediaType.PICTURE,
      allowEdit: false,
      targetHeight: 1000,
      targetWidth: 1000,
      saveToPhotoAlbum : true,
    }
    this.camera.getPicture( options )
      .then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' +  imageData;
        this.image = base64Image;
      }).catch((err) => console.log(err));
  }

  document: string = null;
  abrir_documento(){
    this.fileChooser.open().then(file => {
      this.filePath.resolveNativePath(file).then(resolveFilePath => {
          this.fileOpener.open(resolveFilePath, 'application/pdf').then(file => {
            this.document = resolveFilePath

            let toast = this.toastCtrl.create({
              message : 'el archivo seleccionado fue' + JSON.stringify(resolveFilePath),
              position:'middle',
              duration:3000,
            });

            toast.present();


          }).catch((err) => console.log(err));
      }).catch((err) => console.log(err));
    }).catch((err) => console.log(err));
  }


 data = {};
 id_reg = this.id_registro;
  guardar(){
      var headers = new Headers();
       headers.append("Accept",'application/json');
       headers.append('Content-Type', 'application/x-www-form-urlencoded');
       //options 
        /*let options = new RequestOptions({ headers : headers });*/
        let datos = this.data
        let imagen = {
         imagen : this.image,
         document : this.document,
        } // => datos = {}
        /*let url = '';*/


        let task = this.taskService.guardar_documentos(datos, imagen, this.id_reg)
        if(!task){
          let toast = this.toastCtrl.create({
            message : 'hubo un error guardando los archivos' +  JSON.stringify(task),
            position : 'middle',
            duration : 5000,
          });
          toast.present();
        }else{
           let toast = this.toastCtrl.create({
            message : 'Archivos guardados Correctamente',
            position : 'middle',
            duration : 5000,
          });
          toast.present();
          this.navCtrl.pop();
        }
        

  }

  


 



}
