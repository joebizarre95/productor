var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite } from '@ionic-native/sqlite';
/**
 public sqlite : SQLite
 * Generated class for the ProtocolosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProtocolosPage = /** @class */ (function () {
    function ProtocolosPage(navCtrl, navParams, sqlite) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sqlite = sqlite;
    }
    ProtocolosPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('SELECT * FROM protocolos', {})
                .then(function (response) {
                _this.document = response;
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    ProtocolosPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    ProtocolosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-protocolos',
            templateUrl: 'protocolos.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, SQLite])
    ], ProtocolosPage);
    return ProtocolosPage;
}());
export { ProtocolosPage };
//# sourceMappingURL=protocolos.js.map