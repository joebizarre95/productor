import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';

import { TaskServiceProvider } from '../../providers/task-service/task-service';
import { ToastController } from 'ionic-angular';

/**
 public sqlite : SQLite
 * Generated class for the ProtocolosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-protocolos',
  templateUrl: 'protocolos.html',
})
export class ProtocolosPage {
  document : any;
  data : any ;

  constructor(public navCtrl: NavController, 
                public navParams: NavParams ,
                  public fileChooser : FileChooser,
                    public fileOpener : FileOpener, 
                      public taskService : TaskServiceProvider, 
                        public toastCtrl : ToastController, ) {
  }

  ionViewDidLoad() {
     this.getAllObjects();
  }

  getAllObjects(){
     this.taskService.listarProtocolos()
       .then(protocolos => {
           console.log(protocolos);
           this.data = protocolos

           let toast = this.toastCtrl.create({
             message : 'Cargando Archivos',
             duration: 4000,
             position: 'bottom',
           });

           toast.present();
       })
       .catch(error => {

         let toast = this.toastCtrl.create({
             message : 'Hubo un error cargando los archivos' + JSON.stringify(error),
             duration: 4000,
             position: 'bottom',
           });

           toast.present();

       })
  }




  close(){
  	this.navCtrl.pop();
  }


  openDocument(filename){
    this.fileOpener.open(filename, 'application/pdf')
      .then((file) => console.log('file is opened'))
    .catch(e => console.log('Error opening file',e ));
  }



}
