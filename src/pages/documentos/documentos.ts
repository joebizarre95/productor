import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';

import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
import { TaskServiceProvider } from '../../providers/task-service/task-service';
import { ToastController } from 'ionic-angular';

import { DocumentViewer , DocumentViewerOptions} from '@ionic-native/document-viewer';

/**
 * Generated class for the DocumentosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-documentos',
  templateUrl: 'documentos.html',
})
export class DocumentosPage {

    document : any;
  data : any ;
  constructor(public navCtrl: NavController,
                   public navParams: NavParams ,
                      public fileChooser : FileChooser,
                         public fileOpener : FileOpener,
                            public taskService : TaskServiceProvider, 
                                public toastCtrl : ToastController, 
                                  public documentViewer : DocumentViewer,
                                    public platform : Platform, 
                                      public file : File, ) {
  }


 ionViewDidLoad() {
     this.getAllObjects();
  }

  getAllObjects(){
     this.taskService.listarRegEstaticos()
       .then(biblioteca => {
           console.log(biblioteca);
           this.data = biblioteca

           let toast = this.toastCtrl.create({
             message : 'Cargando Archivos',
             duration: 4000,
             position: 'bottom',
           });

           toast.present();
       })
       .catch(error => {

         let toast = this.toastCtrl.create({
             message : 'Hubo un error cargando los archivos' + JSON.stringify(error),
             duration: 4000,
             position: 'bottom',
           });

           toast.present();

       })
  }



  close(){
  	this.navCtrl.pop();
  }


  openviewer(file){

  let pathFile : string;  

  if(this.platform.is("ios")){
      console.log('ios detected');
      pathFile = this.file.applicationDirectory; //aplicacion file directory
    }else{
      console.log('other platform');
      pathFile = this.file.externalApplicationStorageDirectory;
    }

   let filename : string = file;

   this.file.writeFile(pathFile, filename, '' , {replace:true},)
     .then((entry) =>{
       
       let toast = this.toastCtrl.create({
         message : 'abriendo archivo' + JSON.stringify(pathFile) + JSON.stringify(filename),
         position: 'bottom',
         duration: 4000
       });

       toast.present();
       let options : DocumentViewerOptions = {

       }


       this.documentViewer.viewDocument(pathFile + filename , 'application/pdf', options);

     }).catch((error)=>{
       let toast = this.toastCtrl.create({
         message : 'hubo un error' + JSON.stringify(error),
         position: 'bottom',
         duration : 4000
       });

       toast.present();

     });
        
  }


  ver_documento(document){
     const options : DocumentViewerOptions = {
       title : 'Mi documento'
     }


     this.documentViewer.viewDocument(document, 'application/pdf' , options)
       
  }


}