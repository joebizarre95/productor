import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegmedicosPage } from '../regmedicos/regmedicos';
import { ReganimalesPage } from '../reganimales/reganimales';
import { InventariomedPage } from '../inventariomed/inventariomed';
import { ToastController } from 'ionic-angular';


import { ModalController } from 'ionic-angular';

import { ModalpagePage } from '../modalpage/modalpage';


import { LogRegistrosPage } from '../log-registros/log-registros';


//tabs 

/**
 * Generated class for the RegistrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registros',
  templateUrl: 'registros.html',
})
export class RegistrosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl : ToastController , public modalCtrl : ModalController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrosPage');
  }

  medicamentos(){
  	this.navCtrl.push(RegmedicosPage);
  }

  animales(){
  	this.navCtrl.push(ReganimalesPage);
  }

  inventariomed()
  {
    this.navCtrl.push(InventariomedPage);
  }

  volver(){
    this.navCtrl.pop();
  }


   modal(){
    let modal = this.modalCtrl.create(ModalpagePage);
    modal.present();
  }

  historial(){
    this.navCtrl.push(LogRegistrosPage);
  }


}
