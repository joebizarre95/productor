var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegmedicosPage } from '../regmedicos/regmedicos';
import { ReganimalesPage } from '../reganimales/reganimales';
import { InventariomedPage } from '../inventariomed/inventariomed';
import { ToastController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ModalpagePage } from '../modalpage/modalpage';
import { LogRegistrosPage } from '../log-registros/log-registros';
//tabs 
/**
 * Generated class for the RegistrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegistrosPage = /** @class */ (function () {
    function RegistrosPage(navCtrl, navParams, toastCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
    }
    RegistrosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegistrosPage');
    };
    RegistrosPage.prototype.medicamentos = function () {
        this.navCtrl.push(RegmedicosPage);
    };
    RegistrosPage.prototype.animales = function () {
        this.navCtrl.push(ReganimalesPage);
    };
    RegistrosPage.prototype.inventariomed = function () {
        this.navCtrl.push(InventariomedPage);
    };
    RegistrosPage.prototype.volver = function () {
        this.navCtrl.pop();
    };
    RegistrosPage.prototype.modal = function () {
        var modal = this.modalCtrl.create(ModalpagePage);
        modal.present();
    };
    RegistrosPage.prototype.historial = function () {
        this.navCtrl.push(LogRegistrosPage);
    };
    RegistrosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-registros',
            templateUrl: 'registros.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ToastController, ModalController])
    ], RegistrosPage);
    return RegistrosPage;
}());
export { RegistrosPage };
//# sourceMappingURL=registros.js.map