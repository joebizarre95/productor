import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { ModalController } from 'ionic-angular';

import { ModalpagePage } from '../modalpage/modalpage';




/**
 * Generated class for the RegmedicosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-regmedicos',
  templateUrl: 'regmedicos.html',
})
export class RegmedicosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegmedicosPage');
  }


  guardar(){
  	let toast = this.toastCtrl.create({
  		message: 'Espere mientras se Guardan los Datos',
  		duration: 3000,
  		position:'top'
  	});

  	 toast.onDidDismiss(() => {
 		this.navCtrl.pop();
  });

  toast.present();
  }



  modal(){
    let modal = this.modalCtrl.create(ModalpagePage);
    modal.present();
  }

  

}
