import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegmedicosPage } from './regmedicos';

@NgModule({
  declarations: [
    RegmedicosPage,
  ],
  imports: [
    IonicPageModule.forChild(RegmedicosPage),
  ],
})
export class RegmedicosPageModule {}
