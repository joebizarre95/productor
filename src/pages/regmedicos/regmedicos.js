var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { ModalpagePage } from '../modalpage/modalpage';
/**
 * Generated class for the RegmedicosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegmedicosPage = /** @class */ (function () {
    function RegmedicosPage(navCtrl, navParams, toastCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
    }
    RegmedicosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegmedicosPage');
    };
    RegmedicosPage.prototype.guardar = function () {
        var _this = this;
        var toast = this.toastCtrl.create({
            message: 'Espere mientras se Guardan los Datos',
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(function () {
            _this.navCtrl.pop();
        });
        toast.present();
    };
    RegmedicosPage.prototype.modal = function () {
        var modal = this.modalCtrl.create(ModalpagePage);
        modal.present();
    };
    RegmedicosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-regmedicos',
            templateUrl: 'regmedicos.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ToastController, ModalController])
    ], RegmedicosPage);
    return RegmedicosPage;
}());
export { RegmedicosPage };
//# sourceMappingURL=regmedicos.js.map