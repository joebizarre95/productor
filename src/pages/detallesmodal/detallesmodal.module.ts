import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetallesmodalPage } from './detallesmodal';

@NgModule({
  declarations: [
    DetallesmodalPage,
  ],
  imports: [
    IonicPageModule.forChild(DetallesmodalPage),
  ],
})
export class DetallesmodalPageModule {}
