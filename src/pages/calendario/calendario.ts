import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';

/**
 * Generated class for the CalendarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calendario',
  templateUrl: 'calendario.html',
})
export class CalendarioPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public calendar : Calendar) {
  }

  ionViewDidLoad() {
   this.calendar.createCalendar('MyCalendar').then(
   	(msg) => {
   		console.log(msg);
   	}).catch((error) => {
   		console.log(error);
   	})
  }

}
