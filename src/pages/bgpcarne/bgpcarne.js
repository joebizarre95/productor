var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrosPage } from '../registros/registros';
import { DocumentsPage } from '../documents/documents';
import { ProtocolosPage } from '../protocolos/protocolos';
import { BibliotecaPage } from '../biblioteca/biblioteca';
import { ConfigurationsPage } from '../configurations/configurations';
/**
 * Generated class for the BgpcarnePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BgpcarnePage = /** @class */ (function () {
    function BgpcarnePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BgpcarnePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BgpcarnePage');
    };
    BgpcarnePage.prototype.salir = function () {
        this.navCtrl.pop();
    };
    BgpcarnePage.prototype.registros = function () {
        this.navCtrl.push(RegistrosPage);
    };
    BgpcarnePage.prototype.documentos = function () {
        this.navCtrl.push(DocumentsPage);
    };
    BgpcarnePage.prototype.biblioteca = function () {
        this.navCtrl.push(BibliotecaPage);
    };
    BgpcarnePage.prototype.protocolo = function () {
        this.navCtrl.push(ProtocolosPage);
    };
    BgpcarnePage.prototype.configurations = function () {
        this.navCtrl.push(ConfigurationsPage);
    };
    BgpcarnePage.prototype.log_out = function () {
        this.navCtrl.pop();
    };
    BgpcarnePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-bgpcarne',
            templateUrl: 'bgpcarne.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], BgpcarnePage);
    return BgpcarnePage;
}());
export { BgpcarnePage };
//# sourceMappingURL=bgpcarne.js.map