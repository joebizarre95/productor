import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BgpcarnePage } from './bgpcarne';

@NgModule({
  declarations: [
    BgpcarnePage,
  ],
  imports: [
    IonicPageModule.forChild(BgpcarnePage),
  ],
})
export class BgpcarnePageModule {}
