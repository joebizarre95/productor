import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrosPage } from '../registros/registros';
import { DocumentsPage } from '../documents/documents';
import { DocumentosPage } from '../documentos/documentos';
import { ProtocolosPage } from '../protocolos/protocolos';
import { BibliotecaPage } from '../biblioteca/biblioteca';
import { ConfigurationsPage } from '../configurations/configurations';
/*import { CalendarioPage } from '../calendario/calendario';*/


/**
 * Generated class for the BgpcarnePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bgpcarne',
  templateUrl: 'bgpcarne.html',
})
export class BgpcarnePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BgpcarnePage');
  }

  salir(){
    this.navCtrl.pop();
  }

  registros(){
  	this.navCtrl.push(RegistrosPage);
  }

  documentos(){
      this.navCtrl.push(DocumentosPage);
  }

  biblioteca(){
     this.navCtrl.push(BibliotecaPage);

  }

  protocolo(){
     this.navCtrl.push(ProtocolosPage);
  }

  configurations(){
    this.navCtrl.push(ConfigurationsPage);
  }

  log_out(){
    this.navCtrl.pop();
  }

  /*calendario(){
    this.navCtrl.push(CalendarioPage);
  }*/

}
