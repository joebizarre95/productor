import { HttpClient  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { ToastController } from 'ionic-angular';


import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpProvider {

  constructor(public http: HttpClient, 
              public httpsend : Http ,
              public fileTransfer :  FileTransferObject, 
              public file : File,
              public toastCtrl : ToastController) {
   
  }



  sendpostToServer(datos, url , options ){
      this.http.post(url, datos , options)
                       .subscribe(datos=>{
                        return true
                });
  }

  sendPostWithImage(datos, image, url, options){
    datos = {
      image : image, 
      datos : datos,
    }
    this.http.post(url, datos, options)
              .subscribe(datos=>{
                return true
              });
  }


  datalist = [];
  getdataForUser(url){
     return new Promise(resolve => {
          this.http.get(url).subscribe(data => {
            resolve(data);
          }, err => {
            console.log(err);
          });
        });                
  }


  dowloadDocuments(url){
       this.fileTransfer.download(url, this.file.dataDirectory + 'file.pdf')
       .then((entry) => {
           console.log('dowload complete :' + entry.toURL());
           let toast = this.toastCtrl.create({
             message: 'Descargando archivos',
             duration : 3000,
             position : 'bottom',
           });

           toast.present();

       }).catch((error) => {
         let toast = this.toastCtrl.create({
             message: 'Hubo un error en la descarga de archivos',
             duration : 3000,
              position : 'bottom',
           });

         toast.present();
       })
  }


  uploadDocuments(url, headersoptions){
    let options : FileUploadOptions = {
      fileKey : 'file',
      fileName : 'name.jpg',
      headers : headersoptions
    }

    this.fileTransfer.upload('<file path>', '<api endpoint>', options)
      .then((data) => {
          let toast = this.toastCtrl.create({
             message: 'Archivo Subido Correctamente',
             duration : 3000, 
              position : 'bottom',
           });

          toast.present();

      }).catch((error) => {
        let toast = this.toastCtrl.create({
             message: 'Hubo un error en la subida del archivo' + error ,
             duration : 3000, 
              position : 'bottom',
           });

        toast.present();
      })
  }







}
