var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
/*
  Generated class for the TaskServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var TaskServiceProvider = /** @class */ (function () {
    function TaskServiceProvider(http, sqlite, toast) {
        this.http = http;
        this.sqlite = sqlite;
        this.toast = toast;
        this.db = null;
    }
    TaskServiceProvider.prototype.open_database = function () {
        var _this = this;
        this.sqlite.create({
            name: 'data.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('CREATE TABLE IF NOT EXIST api_userlog( id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, datelog TEXT)', {})
                .then(function (db) {
                db.executeSql('CREATE TABLE IF NOT EXIST documents( id INTEGER PRIMARY KEY AUTOINCREMENT , document VARCHAR(255) , actualizado VARCHAR(255),)', {})
                    .then(function (db) {
                    db.executeSql('CREATE TABLE IF NOT EXIST protocolos( id INTEGER PRIMARY KEY AUTOINCREMENT, document VARCHAR(255), actualizado VARCHAR(255),)', {})
                        .then(function (db) {
                        db.executeSql('CREATE TABLE IF NOT EXIST biblioteca( id INTEGER PRIMARY KEY AUTOINCREMENT, document VARCHAR(255), actualizado VARCHAR(255),)', {});
                        var toast = _this.toast.create({
                            message: 'base de datos Inicializada',
                            duration: 3000,
                            position: 'bottom',
                        });
                        toast.present();
                    }).catch(function (error) {
                        var toast = _this.toast.create({
                            message: 'Hubo un error al abrir la base de datos',
                            duration: 3000,
                            position: 'bottom',
                        });
                        toast.present();
                    });
                }).catch(function (error) {
                    var toast = _this.toast.create({
                        message: 'Hubo un error al abrir la base de datos',
                        duration: 3000,
                        position: 'bottom',
                    });
                    toast.present();
                });
            }).catch(function (error) {
                var toast = _this.toast.create({
                    message: 'Hubo un error al abrir la base de datos',
                    duration: 3000,
                    position: 'bottom',
                });
                toast.present();
            });
        }).catch(function (error) {
            var toast = _this.toast.create({
                message: 'Hubo un error al abrir la base de datos',
                duration: 3000,
                position: 'bottom',
            });
            toast.present();
        });
    };
    //estructura para crear las tablas
    TaskServiceProvider.prototype.getAllLogs = function () {
        var sql = 'SELECT * FROM api_userlog LIMIT 1';
        return this.db.executeSql(sql, []).then(function (response) {
            var api_userlog = [];
            for (var index = 0; index < response.rows.length; index++) {
                api_userlog.push(response.rows.item(index));
            }
            return Promise.resolve(api_userlog);
        }).catch(function (error) { return Promise.reject(error); });
    };
    TaskServiceProvider.prototype.create_logs = function (api_userlog) {
        var sql = 'INSERT INTO api_userlog (username, password)VALUES(?,?)';
        return this.db.executeSql(sql, [api_userlog.username, api_userlog.password]);
    };
    TaskServiceProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, SQLite, ToastController])
    ], TaskServiceProvider);
    return TaskServiceProvider;
}());
export { TaskServiceProvider };
//# sourceMappingURL=task-service.js.map