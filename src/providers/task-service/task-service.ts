import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite , SQLiteObject } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
/*
  Generated class for the TaskServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaskServiceProvider {

  constructor(public http: HttpClient , public sqlite : SQLite , public toast : ToastController) {
    
  }

  db: SQLiteObject = null;
  setDatabase(db : SQLiteObject){
    if(this.db === null){
      this.db = db;
    }
  }



  

//||||||||||||||||||||||||||||||||||||||||||| TABLAS NUEVAS ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //tabla referencial a la data del usuario actual
  TableUser(){
   let sql = 'CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, id_productor INTEGER , nomre_finca varchar(255), ubicacion_finca varchar(255), estado_finca varchar(255) )';
    return this.db.executeSql(sql,[]); 
  }
  TableNormasUser(){
    let sql = 'CREATE TABLE IF NOT EXISTS normas_user(norma_id varchar (255) NULL, productor_id varchar (255) NULL )';
     return this.db.executeSql(sql, []); 
  }
  //re significa registro estatico
  RegEstatic(){
    let sql = 'CREATE TABLE IF NOT EXISTS documentos(id INTEGER PRIMARY KEY AUTOINCREMENT, archivo_nombre varchar(255)  NULL, ubicacion varchar(255)  NULL, fecha_descarga varchar(255) )';
    return this.db.executeSql(sql, []); 
  }

  ResponseReg(){
   
  }
  /*
  version archivo se incrementa por el id del registro
  tipos de archivo 1: excel, 2 pdf, 3 world, 4 imagen, 5 otro 
  estado 1 validado 2 sin validar 
  */
  RegEstaticArchivos(){
    /*let sql = 'CREATE TABLE IF NOT EXISTS response_archivo(id INTEGER PRIMARY KEY AUTOINCREMENT, direccion_archivo varchar(255)  NULL, comentario varchar(255) NULL, observacion varchar(255) NULL,  fecha_vencimiento varchar(255) NULL,  estado varchar(255)  NULL,  norma_id varchar(255)  NULL)';
    return this.db.executeSql(sql, []);*/
  }
  TableProtocolo(){
   let sql = 'CREATE TABLE IF NOT EXISTS protocolos(id INTEGER PRIMARY KEY AUTOINCREMENT, archivo_nombre varchar(255)  NULL, ubicacion varchar(255)  NULL, fecha_descarga varchar(255) )';
    return this.db.executeSql(sql, []); 
  }
   TableBiblioteca(){
    let sql = 'CREATE TABLE IF NOT EXISTS biblioteca(id INTEGER PRIMARY KEY AUTOINCREMENT, archivo_nombre varchar(255)  NULL, ubicacion varchar(255)  NULL, fecha_descarga varchar(255) )';
    return this.db.executeSql(sql, []); 
  }

 // ||||||||||||||||||||||||||||||||||||||||||||||||| INSERTS Y ACCIONES PARA LAS TABLAS ||||||||||||||||||||||||||||||||||||

   InsertUser(data : any){
  /*  let sql = 'INSERT INTO user (username, id_productor, nombre_finca, ubicacion_finca, estado_finca)VALUES(?,?,?,?,?)';
    return this.db.executeSql(sql,[data.username, data.id_productor, data.nombre_finca, data.ubicacion_finca, data.estado_finca]);
    */
   }



newData(){
    
  }

 guardar_documentos(datos: any, imagen, id_reg){ 
     
  }


  newData2(){
    let sql = 'INSERT INTO biblioteca(archivo_nombre, ubicacion, fecha_descarga) VALUES ("Documento de Bienvenida","assets/miarchivo.pdf","12/11/2012")';
     return this.db.executeSql(sql, []); 
  }

  newData3(){
    let sql = 'INSERT INTO protocolos(archivo_nombre, ubicacion, fecha_descarga) VALUES ("Documento de Bienvenida","assets/miarchivo.pdf","12/11/2012")';
     return this.db.executeSql(sql, []); 
  }


 newData4(){
  let sql = 'INSERT INTO documentos(archivo_nombre, ubicacion, fecha_descarga) VALUES ("Documento de Bienvenida","assets/miarchivo.pdf","12/11/2012")';
     return this.db.executeSql(sql, []); 
  }

  newData5(){
   /*let sql = 'INSERT INTO response_archivo(direccion_archivo, comentario, observacion, fecha_vencimiento, estado , norma_id ) VALUES ("assets/miarchivo.pdf","micomentario","miobservacion","12/12/2018","verificado","1")';
     return this.db.executeSql(sql, []);*/
  }


 






//|||||||||||||||||||||||||||||||||||||||||||||||||||||| CONSULTAS |||||||||||||||||||||||||||||||||||||||||||||||||

    listarProtocolos(){
      let sql = "SELECT * FROM protocolos";
      return this.db.executeSql(sql, [])
        .then(response => {
          let protocolos = [];
          for(let index = 0; index < response.rows.length; index++){
              protocolos.push(response.rows.item(index));
            }
            return Promise.resolve(protocolos);
        })
        .catch(error => Promise.reject(error));
    }   


    listarBiblioteca(){
      let sql = 'SELECT * FROM biblioteca';
      return this.db.executeSql(sql, [])
        .then(response => {
          let protocolos = [];
          for(let index = 0; index < response.rows.length; index++){
              protocolos.push(response.rows.item(index));
            }
            return Promise.resolve(protocolos);
        })
        .catch(error => Promise.reject(error));
    }   
 
    listarRegEstaticos(){
      let sql = 'SELECT * FROM documentos';
      return this.db.executeSql(sql, [])
        .then(response => {
          let archivos = [];
          for(let index = 0; index < response.rows.lenght; index++){
            archivos.push(response.rows.item(archivos));
          }
          return Promise.resolve(archivos);
        })
        .catch(error => Promise.reject(error));
    }

    listarDocumentosxReg(){
       let sql = 'SELECT * FROM response_archivo';
      return this.db.executeSql(sql, [])
        .then(response => {
          let archivos = [];
          for(let index = 0; index < response.rows.lenght; index++){
            archivos.push(response.rows.item(archivos));
          }
          return Promise.resolve(archivos);
        })
        .catch(error => Promise.reject(error));

    }


    //|||||||||||||||||||||||||||||||||||||||||||||||||||| ALMACENAMIENTOS |||||||||||||||||||||||||||||||||||||||||||
















}


