import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { SQLite , SQLiteObject } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
/*
  Generated class for the ExportProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ExportProvider {

  constructor(public http: HttpClient, 
  				public httpsend : Http, 
  					public sqlite : SQLite, 
  						public fileTransfer :  FileTransferObject, 
              				public file : File,
                        public toastCtr : ToastController, ) {
   
  }



  documents : any;
  headersoptions = {

  }
  i : any;
  exporting_documents(){
  	this.sqlite.create({
  		name : 'data.db',
  		location: 'default',

  	}).then((db :  SQLiteObject) => {
  		db.executeSql('SELECT document FROM documents', {})
  			.then((result) => {
  				this.documents = result;

  			for(this.i = 0 ; this.documents < 0 ; this.i ++){

	  				let options : FileUploadOptions = {
	  					 fileKey : 'file',
					      fileName : 'name.jpg',
					      headers : this.headersoptions,
	  				}

	  				this.fileTransfer.upload(this.documents , '<pi endpoint>', options)
	  					.then((data) => {
	  						console.log('Archivo Subido');
	  					}).catch((error) => {
	  						console.log(error);
	  					})
  				}
  			}).catch((error) => {
  				console.log(error);
  			})

  	})
  }

}
